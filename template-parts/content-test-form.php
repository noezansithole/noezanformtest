<?php
?>
<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-md-6 col-md-offset-4">
            <div class="form-wrapper mt-4" >
                <form method="post" class="ajax" enctype="multipart/form-data" id="form" action="">
                    <div class="card">
                      <div class="card-header">
                        <div class="form-heading">
                            <h3 class="title">Test</h3>
                        </div>
                      </div>

                        <div class="card-body">
                            <div id="response"></div>
                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="field-label col-sm-2 col-form-label" for="input-1">Input 1</label>
                                    <div class="col-sm-10">
                                        <input class="form-control-plaintext" placeholder="Name field" name="input-1" id="input-1" type="text" value="" aria-invalid="false">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="field-label col-sm-2 col-form-label" for="input-1">Input 2</label>
                                    <div class="col-sm-10">
                                        <input class="form-control-plaintext" placeholder="Email field" name="input-2" id="input-2" type="text" value="" aria-invalid="false">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="field-label col-sm-2 col-form-label" for="input-3">Input 3</label>
                                    <div class="col-sm-10">
                                        <input class="form-control-plaintext" placeholder="Message field" name="input-3" id="input-3" type="text" value="" aria-invalid="false">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="form-footer">
                    	        <input type="submit"
                    	               id="form-submit-button"
                    	               class="button"
                    	               value="Submit"
                    	               onclick="" />
                            </div>
                        </div>
                
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

