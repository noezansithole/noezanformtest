<?php

/**
 * WordPress
 */
require_once ( get_template_directory() . '/inc/wp-add-theme-support.php' );
require_once ( get_template_directory() . '/inc/wp-enqueue-styles.php' );

// embed the javascript file that makes the AJAX request
wp_enqueue_script( 'form-theme-ajax','/wp-content/themes/form-theme/assets/js/main.js', array( 'jquery' ) );

function ajax_form_scripts() {
	wp_localize_script( 'form-theme-ajax', 'cpm_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'ajax_form_scripts' );

add_action( 'wp_ajax_set_form', 'set_form' );    //execute when wp logged in
add_action( 'wp_ajax_nopriv_set_form', 'set_form'); //execute when logged out

function set_form(){
	##CREATE AJAX OBJECT
	$name = $_POST['name'];
	$email = $_POST['email'];
	$message = $_POST['message'];
	
	$return = array(
    	'name'  => $name,
    	'email'  => $email,
    	'message' => $message
	);
	wp_send_json($return);
	die();
}

