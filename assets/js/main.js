
   jQuery(document).ready(function($){
	$('form.ajax').on('submit', function(e){

	   e.preventDefault();
	      
	   		var that = $(this),
		   	url = that.attr('action'),
		   	type = that.attr('method');
		   	var name = $('#input-1').val();
		   	var email = $('#input-2').val();
		   	var message = $('#input-3').val();
		   
		   	$.ajax({
				url: cpm_object.ajax_url,
				type:"POST",
				dataType: "json",
				data: {
				 	action:'set_form',
				 	name:name,
				 	email:email,
				 	message:message,
				},  success: function(response){
					$( "#response" ).empty();
					$( "#response" ).append( printalerts("Name",response["name"]));
					$( "#response" ).append( printalerts("Email",response["email"]));
					$( "#response" ).append( printalerts("Message",response["message"]));
				}, error: function(data){
 					$( "#response" ).empty().append( "<p class='alert alert-danger'>Error!</p>" );     
 				}
		   	});
		$('.ajax')[0].reset();
	  	});
	});

	function printalerts(name,value){
		if(value && value != ''){
			return "<div class='alert alert-success'>Your <b>"+name+"</b> is <u>"+value+"</u></div>"; 	
		}else{
			return "<div class='alert alert-danger'>Could not find <b>"+name+"</b></div>"; 
		}
	}
